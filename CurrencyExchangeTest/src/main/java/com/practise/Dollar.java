package com.practise;

public class Dollar extends Money {

    public Dollar(int amount, String currency) {
        super(amount, currency);
    }

    @Override
    public boolean equals(Object obj) {
        return this.amount == ((Money)obj).amount;
    }
}
