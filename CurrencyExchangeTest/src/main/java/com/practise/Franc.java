package com.practise;

public class Franc extends Money {

    public Franc(int amount, String currency) {
        super(amount, currency);
    }

    @Override
    public boolean equals(Object obj) {
        return this.amount == ((Money)obj).amount;
    }
}
