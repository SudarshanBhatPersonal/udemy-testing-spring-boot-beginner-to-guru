package com.practise;

import java.util.Objects;

public class Pair {
    private final String from;
    private final String to;
    public Pair(String f, String t){
        from = f; to = t;
    }
    @Override
    public int hashCode() {
        return Objects.hash(from, to);
    }

    @Override
    public boolean equals(Object o) {
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;
        Pair pair = (Pair)o;
        return Objects.equals(from, pair.from) && Objects.equals(to, pair.to);
    }
}
