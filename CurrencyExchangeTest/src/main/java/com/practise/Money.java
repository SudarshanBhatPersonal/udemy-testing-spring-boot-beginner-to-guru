package com.practise;
/*
problem statement:
    . You are supposed to design a currency multiplier application.
     for a given currency, get its X times value. in a step by step TDD way
     improving the code.
    . Goal is learn the TDD.
    . Every TDD change has to be reflected in a separate commit.


*/



public class Money implements Expression {
    protected int amount;
    protected String currency;

    public Money times(int multiplier){
        return new Money(amount * multiplier, currency);
    }
    protected String currency() {
        return currency;
    }
    public Money(int amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }
    //    public static Money dollar(int amount) {
    //        return new Dollar(amount, "USD");
    //    }
    //    public static Money franc(int amount) {
    //        return new Franc(amount, "CHF");
    //    }
    public static Money dollar(int amount) {
        return new Money(amount, "USD");
    }
    public static Money franc(int amount) {
        return new Money(amount, "CHF");
    }
    @Override
    public boolean equals(Object o) {
        return this.getClass().equals(o.getClass()) && this.amount == ((Money) o).amount;

    }
    //@Override
    public Expression plus(Expression addend) {
        return new Sum(this, addend);
    }

    @Override
    public Money reduce(Bank bank, String to) {
        // int rate = (currency.equals("CHF") && to.equals("USD")? 2: 1 );
        return new Money(amount / bank.rate(this.currency, to) , to);
    }
}
