package guru.springframework.sfgpetclinic.services.springdatajpa;

import guru.springframework.sfgpetclinic.model.Speciality;
import guru.springframework.sfgpetclinic.repositories.SpecialtyRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.inject.Inject;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.*;
import static org.mockito.Mockito.*;

// NOTE: START REFERING THE CODE FROM THE BOTTOM. IT ALL STARTED THERE.


@ExtendWith(MockitoExtension.class)
class SpecialitySDJpaServiceTest {

    @Mock(lenient = true)
    SpecialtyRepository specialtyRepository;

    @InjectMocks
    SpecialitySDJpaService service;


    @Test
    void testDeleteByobject() {
        // using the Argument Matchers.
        Speciality speciality = new Speciality();
        service.delete(speciality);
        verify(specialtyRepository).delete(any(Speciality.class));
    }
    @Test
    void findByIdTest() {
        Speciality speciality = new Speciality();
        when(specialtyRepository.findById(1L)).thenReturn(Optional.of(speciality));
        speciality = service.findById(1L);

        specialtyRepository.findById(1L);

        // Assert that the returned object is not NULL and Verify how many times
        // it gets called.
        assertThat(speciality).isNotNull();
        verify(specialtyRepository, atMost(2)).findById(1L);
    }

    // BDD: BEHAVIOUR DRIVEN TESTING.
    // MOCKITO TIMEOUT TESTING
    @Test
    void findByIdBddTest() {
        Speciality speciality = new Speciality();
        given(specialtyRepository.findById(1L)).willReturn(Optional.of(speciality));

        Speciality foundSpecialty = service.findById(1L);
//        assertThat(foundSpecialty).isNotNull();
//        verify(specialtyRepository).findById(anyLong()); // implies, specialtyRepository is called exactly once.
        then(specialtyRepository).should().findById(anyLong());
        then(specialtyRepository).should(timeout(20).times(1)).findById(anyLong());
        then(specialtyRepository).shouldHaveNoMoreInteractions();  // This is the termination method.
        // NOTE: you don't need given().willReturn to write ][ then().should(times() ).METHOD();
    }
    @Test
    void deleteById() {
        service.deleteById(1l);
        service.deleteById(1l);
        verify(specialtyRepository, times(2)).deleteById(1l);
    }

    @Test
    void deleteByIdAtleastOnce() {
        service.deleteById(1l);
        service.deleteById(1l);
        verify(specialtyRepository, timeout(1).atLeastOnce()).deleteById(1l);
        verify(specialtyRepository, atMost(3)).deleteById(1l);
        verify(specialtyRepository, never()).deleteById(4l);

    }

    @Test
    void testDelete() {
        service.delete(new Speciality());
    }

    @DisplayName("Test To Throw")
    @Test
    void testToThrow() {
        // doThrow is used when you are dealing with Void return types for method calls.
        doThrow(new RuntimeException("boom")).when(specialtyRepository).delete(any());
        assertThrows(RuntimeException.class, () -> specialtyRepository.delete(new Speciality()));
        verify(specialtyRepository, times(1)).delete(any());
    }

    @DisplayName("BDD Test To Throw")
    @Test
    void BddTestToThrow() {
        willThrow(new RuntimeException("Boom")).given(specialtyRepository).findById(anyLong());
        assertThrows(RuntimeException.class, () -> specialtyRepository.findById(1L));
        then(specialtyRepository).should().findById(1L);
    }

    @DisplayName("Test delete Bdd")
    @Test
    void testDeleteBdd() {
        willThrow(new RuntimeException("boom")).given(specialtyRepository).findById(anyLong());
        assertThrows(RuntimeException.class, () -> specialtyRepository.findById(1L));
        then(specialtyRepository).should().findById(1L); // I DID NOT UNDERSTAND THIS STATEMENT.
        // WHY DO I NEED TO ASSERT prior to then();?
    }

    @DisplayName("Test using Lambda in BDD")
    @Test
    void testSaveLambda() {
        final String MATCH_ME = "MATCH_ME";
        final String NOT_MATCH = "NOT_MATCH";
        Speciality speciality = new Speciality();
        speciality.setDescription(MATCH_ME);

        Speciality savedSpecialty = new Speciality();
        savedSpecialty.setId(1L);

        given(specialtyRepository.save(argThat(argument -> argument.getDescription().equals(NOT_MATCH))))
                .willReturn(savedSpecialty);
        // when
        Speciality returnedSpecialty = service.save(speciality);
        // then
        // assertThat(returnedSpecialty.getId()).isEqualTo(1L);
        assertNull(returnedSpecialty);  // FOR THIS TO HAPPEN ADDED     @Mock(lenient= true)
    }


}