package guru.springframework.sfgpetclinic.controllers;

import guru.springframework.sfgpetclinic.fauxspring.BindingResult;

import guru.springframework.sfgpetclinic.fauxspring.Model;
import guru.springframework.sfgpetclinic.model.Owner;
import guru.springframework.sfgpetclinic.services.OwnerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

// @ExtendWith(MockitoExtension.class) // EITHER USE SETUP OR THIS @ANNOTATION.
class OwnerControllerTest {

    @InjectMocks
    OwnerController controller;

    @Mock
    Model model;

    @Mock
    OwnerService service;

    @Mock
    BindingResult bindingResult;

    // METHOD: 2
    @Captor
    ArgumentCaptor<String> stringArgumentCaptor;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }


    // ARGUMENT CAPTOR PRACTISE
    // INORDER , VERIFYING THE ORDER OF METHOD CALLS.
    @Test
    void processFindFormWildcardString() {
        Owner owner = new Owner(1l, "Joe", "Buck");
        List<Owner> ownerList = new ArrayList<>();
        ownerList.add(owner);
        InOrder order = inOrder(service, model); //
        // final ArgumentCaptor<String> stringArgumentCaptor = ArgumentCaptor.forClass(String.class);  // METHOD 1

        // given
        given(service.findAllByLastNameLike(stringArgumentCaptor.capture())).willReturn(ownerList);

        // when
        String viewName = controller.processFindForm(owner, bindingResult, model);

        // then
        assertThat("%Buck%").isEqualToIgnoringCase(stringArgumentCaptor.getValue());

        order.verify(service).findAllByLastNameLike(anyString());
        //order.verify(model).addAttribute(anyString(), anyList());
        verifyNoMoreInteractions(model);

    }

    // PRACTISE FOR Answer<>()
    @Test
    @DisplayName("Answer<> Invocation Practise")
    void processFindFormWildcardNotFound() {
        //given
        Owner owner = new Owner(1l, "Joe", "Dont find me");
        List<Owner> ownerList = new ArrayList<>();
        ownerList.add(owner);

        final ArgumentCaptor<String> stringArgumentCaptor = ArgumentCaptor.forClass(String.class);

        when(service.findAllByLastNameLike(stringArgumentCaptor.capture())).thenAnswer(invocationOnMock -> ownerList);

        String viewName = controller.processFindForm(owner, bindingResult, null);

        // then
        assertThat("%Dont find me%").isEqualToIgnoringCase(stringArgumentCaptor.getValue());
        assertThat("redirect:/owners/1").isEqualToIgnoringCase(viewName);
    }


}