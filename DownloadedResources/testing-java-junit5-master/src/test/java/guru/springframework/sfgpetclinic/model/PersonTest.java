package guru.springframework.sfgpetclinic.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PersonTest {
    Person person;
    @BeforeEach
    void setUp() {
        person = new Person(1l, "Joe", "Blank");
    }

    @Test
    void groupedAssertions() {

        assertAll("Test props Set",
                () -> assertEquals("Joe", person.getFirstName()),
                () -> assertTrue("Blank".equals(person.getLastName()), () -> "last name is wrong"));
    }

    @Test
    void groupedAssertionMsgs() {

    }
}