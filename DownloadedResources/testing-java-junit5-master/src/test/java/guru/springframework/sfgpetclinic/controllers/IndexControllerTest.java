package guru.springframework.sfgpetclinic.controllers;

import guru.springframework.sfgpetclinic.Exceptions.ValueNotFoundException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.*;

import java.time.Duration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

class IndexControllerTest {

    IndexController indexController;

    @BeforeEach
    void setUp() {
        indexController = new IndexController();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    @DisplayName("Test proper value name")
    void index() {
        assertEquals("index", indexController.index(), ()-> "wrong string returned");
        assertThat(indexController.index()).isEqualTo("index");
    }

    @Test
    @DisplayName("Test Exception")
    void oupsHandler() {
        assertThrows(ValueNotFoundException.class, () -> indexController.oupsHandler());
        // assertTrue("asdf".equals(indexController.oupsHandler()), () -> "This is Some Expensive " + "message to build for my test!");
    }

    @Test
    @DisplayName("NON-Preemptive timeout test")
    void testTimeout() {
        assertTimeout(Duration.ofMillis(2000), () -> {
            Thread.sleep(500);
            System.out.println("NON preemptive Assertion running on the same thread");
        });
    }

    @Test
    @DisplayName("Preemptive timeout test")
    void testTimeoutPreemptively() {
        assertTimeoutPreemptively(Duration.ofMillis(2000), () -> {
            Thread.sleep(300);
            System.out.println("This is preemptive Assertion. Running on a different Thread and This " +
                    "will get terminated if not executed within asserted time.!");
        });
    }

    @Test
    @DisplayName("AssumeTrue Test")
    void assumeTrueTest() {
        System.getenv().entrySet().stream().forEach(es -> System.out.println(es.getKey() + " == " + es.getValue()));
        assumeTrue("GURU".equalsIgnoreCase("GURU"));
    }

    @Test
    @DisplayName(value="Java_8 conditional Test")
    // @EnabledOnOs(OS.WINDOWS)
    // @EnabledOnJre(JRE.JAVA_10)
    @EnabledIfEnvironmentVariable(named = "NUMBER_OF_PROCESSORS", matches="4")
    void java8Test() {
        System.out.println("This test case executed as you are running JRE 1.8");
        assertTrue(true);
    }
}