package guru.springframework.sfgpetclinic.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OwnerTest {

    @BeforeEach
    void setUp() {
    }
    @Test
    void dependentAssertions() {
        Owner owner = new Owner(1l, "Joe", "Buck");
        owner.setCity("Key West");
        owner.setTelephone("4950-495-03");

        assertAll("Properties Test",
                () -> assertAll( "Person Properties",
                        () -> assertEquals("Joe", owner.getFirstName()),
                        () -> assertEquals("Buck", owner.getLastName())
                ),
                () -> assertAll( "Owner Properties",
                        () -> assertEquals("Key West", owner.getCity()),
                        () -> assertEquals("4950-495-03", owner.getTelephone())
                )
        );
    }
}